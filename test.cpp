//============================================================================
// Name        : test.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <memory>

using namespace std;

void abort()
{
  throw std::string{"Error"};
}

class Tree
{
  std::string name;

  Tree* parent;
  std::vector<Tree*> children;

public:

  Tree(std::string&& name_);

  virtual ~Tree();

  std::string getName() const;

  void setParent(Tree* parent);

  Tree* getParent() const;
  Tree* getGrandParent() const;
  std::vector<Tree*> getChildren();

  void addChild(Tree* newChild);

  bool isLeaf() const;
  bool isRoot() const;
};

Tree::Tree(std::string&& _name) : name{ _name }, parent{ nullptr }
{

}

void Tree::addChild(Tree* newChild)
{
  // Check if the child is valid
  if (newChild == nullptr)
  {
    cout << "Wrong pointer" << endl;
    abort();
  }

  // Check that the child is not already in a tree.
  if (newChild->parent != nullptr)
  {
    cout << "This node is already in a tree" << endl;
    abort();
  }

  // Check that if the child with that name is not already in a tree.
  for(auto& child : children)
  {
    if(newChild->getName() == child->getName())
    {
      cout << "Object with the same name exist" << endl;
      abort();
    }
  }

  // Add it to the tree.
  newChild->parent = this;
  children.push_back(newChild);
}

std::vector<Tree*> Tree::getChildren()
{
  return children;
}

Tree::~Tree()
{
  children.clear();
}

std::string Tree::getName() const
{
  return name;
}

void Tree::setParent(Tree* _parent)
{
  parent = _parent;
}

Tree* Tree::getParent() const
{
  return parent;
}

Tree* Tree::getGrandParent() const
{
  return parent->getParent();
}

bool Tree::isLeaf() const
{
  return (children.size() == 0);
}

bool Tree::isRoot() const
{
  return (parent == nullptr);
}

/*
            A
         /  |  \
        /   |   \
      aa    bb   cc
     / \     |
    a   b    c

*/


/*
 *
 */
class LcdHd44780
{
  unsigned int x {0};
  unsigned int y {0};

  unsigned int rows;
  unsigned int colums;

public:

  LcdHd44780(unsigned int _rows, unsigned int _colums) :
  rows(_rows), colums(_colums)
  {

  }

  LcdHd44780() = delete;

  void print(std::string&& s)
  {
    for(decltype(x) space = 0; space < x; space++)
    {
      cout << " ";
    }

    for(decltype(x) xpos = x; x < (colums - xpos); xpos++ )
    {
      cout << s[xpos];
    }
    cout << endl;

    x = 0; y = 0;
  }

  void setCursor(unsigned int _x, unsigned int _y)
  {
    _x > colums ? _x = colums : x = _x;
    _y > rows   ? _y = rows   : y = _y;

    for(decltype(y) lines = 0; lines < y; lines++)
    {
      cout << endl;
      if(lines == rows) break;
    }
  }

  void clear()
  {
    cout.clear();
  }
};


class Menu
{
  std::vector<Tree*> menu;

  unsigned int pointer{};
  unsigned int layers{};

  LcdHd44780& lcd;


  const std::string   selectSymbol{"* "};
  const std::string noSelectSymbol{"  "};

public:

  void printMenu(std::vector<Tree*> actualMenu)
  {
    std::string menuElement{};

    for (std::size_t i = 0; i < actualMenu.size(); i++)
    {
      (i == pointer) ? menuElement = selectSymbol : menuElement = noSelectSymbol;
      menuElement += actualMenu[i]->getName();
      cout << menuElement << endl;
    }
  }

  Menu(std::vector<Tree*> _menu, LcdHd44780& _lcd) : menu{_menu}, lcd{_lcd}
  {
    printMenu(menu);
  }

  void next()
  {
    pointer == (menu.size() - 1) ? pointer = 0 : pointer++;
    printMenu(menu);
  }

  void prev()
  {
    pointer == 0 ? pointer = (menu.size() - 1) : pointer--;
    printMenu(menu);
  }

  void enter()
  {
    menu = menu[pointer]->getChildren();

    if (!menu.empty())
    {
      printMenu(menu);
      layers++;
    }

    else
    {
      // if no child call functor connected
      // to children[pointer] element
    }
  }

  void back()
  {
    if (layers > 0)
    {
      layers--;
      menu = menu[pointer]->getGrandParent()->getChildren();
      printMenu(menu);
    }
  }
};

void printMenu(vector<Tree*> menuList)
{
  for (auto& menu : menuList)
  {
    cout << menu->getName() << endl;
  }
}

int main()
{

  Tree A
  { "A" };

  Tree aa("aa"),
       a("a"),
       b("b");
  Tree bb("bb"),
       c("c");
  Tree cc("cc");

  A.addChild(&aa);

  aa.addChild(&a);
  aa.addChild(&b);

  A.addChild(&bb);
  bb.addChild(&c);

  A.addChild(&cc);



  LcdHd44780 lcd{2, 16};

  Menu menu{A.getChildren(), lcd};

  char cmd{};

  while (1)
  {
    cin >> cmd;

    if (cmd == 'n') menu.next();
    else if (cmd == 'p') menu.prev();
    else if (cmd == 'e') menu.enter();
    else if (cmd == 'b') menu.back();
    else break;

  }

  return 0;
}









